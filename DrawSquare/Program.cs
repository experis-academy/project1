﻿using System;

namespace DrawSquare
{
    class Program
    {
        static void Main(string[] args)
        {
            //square made of * outlines
            //declare number
            int no = 0;
            //prompt no of rows and cols
            do
            {
                //handling possible errors
                try
                {
                    Console.WriteLine("Enter number of rows and columns: ");

                    no = int.Parse(Console.ReadLine());
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    Console.WriteLine("Enter a positive number.");
                }
            }
            while (no < 1);//prompt until you get positive integer

            //top row
            for (int i = 0; i < no; i++)
            {
                Console.Write(" *");
            }
            Console.WriteLine();
            //middle rows
            for (int i = 0; i < no - 2; i++)
            {
                for (int j = 0; j < no; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(" *");
                    }
                    else if (j == no - 1)
                    {
                        Console.WriteLine(" *");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
            }
            //bottom row
            for (int i = 0; i < no; i++)
            {
                Console.Write(" *");
            }
        }
    }
}
